import React from 'react'
import Dashboard from './../Components/Dashboard/Dashboard'
import Landing from './../Components/Landing/Landing'

import { URL_DASHBOARD, URL_LANDING, URL_BASE } from './../Helpers/Paths'

export default [
    {
        path : URL_BASE,
        exact : true,
        component : () => <Landing />
    },
    {
        path : URL_LANDING,
        exact : true,
        component : () => <Landing />
    },
    {
        path : URL_DASHBOARD,
        exact : true,
        component : () => <Dashboard />
    },
]