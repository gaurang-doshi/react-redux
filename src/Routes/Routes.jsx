import React from 'react'
import { Switch, Route } from 'react-router-dom'

import RouterList from './RoutesList'

class Routes extends React.Component{
    render(){
        return(
            <Switch>
                {
                    RouterList.map((route, index) => (
                        <Route
                            key={index}
                            path={route.path}
                            exact={route.exact}
                            component={route.component}
                        />
                    ))
                }
            </Switch>
        )
    }
}

export default Routes;
