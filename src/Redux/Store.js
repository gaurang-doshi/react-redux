import { applyMiddleware, combineReducers, compose, createStore } from 'redux'
import Reducers from './Reducers'

const env = process.env.REACT_APP_ENV;
const middleware = []
const composeEnhancers = 
    env !== 'prod' &&
    typeof window === "object" &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
        ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
            // Specify here name, actionsBlacklist, action
        })
        :
        compose

const store = createStore(
    combineReducers({
        ...Reducers
    }),
    composeEnhancers(applyMiddleware(...middleware))
)

export { store }
