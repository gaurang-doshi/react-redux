import { ACTION_TYPE } from './Actions'

const initialState = {
    isLoggedIn : false,
    userData : null,
    userToken : ''
}

const AuthReducer = (state = initialState, action) => {
    switch(action.type){
        case ACTION_TYPE.LOGIN_USER:
            return{
                ...state,
                userData : action.userData,
                userToken : action.userToken,
                isLoggedIn : true
            }
        case ACTION_TYPE.LOGOUT_USER:
            return{
                ...state,
                isLoggedIn : false
            }
        default:
            return state;
    }
}

export default AuthReducer