import axios from "axios";
import { notification } from 'antd'

const METHOD = {
    GET: "get",
    POST: "post",
    PUT: "put",
    DELETE: "delete",
}

const BASEURL = ''; // Local URL

// CHECK BELOW FOR SAMPLE DATA TO BE PASSED
class API {
    isLoggedIn = false;
    userData = {};
    userToken = null;

    constructor() {
        this.baseURL = BASEURL;
        this.getAuthenticationInfo();
    }

    getAuthenticationInfo() {
        if (localStorage.getItem('isLoggedIn')) {
            this.isLoggedIn = true;
            this.userData = JSON.parse(localStorage.getItem('userData'))
            this.userToken = localStorage.getItem('userToken');
        }
    }

    // URL FOR API
    // REFER SAMPLE JSON AT BOTTOM FOR DATA VALUES
    get(url, data) {
        return new Promise((resolve, reject) => {
            this.api(METHOD.GET, url, data)
                .then(response => {
                    resolve(response)
                }).catch(error => {
                    console.log(error);
                })
        })
    }

    post(url, data) {
        return new Promise((resolve, reject) => {
            this.api(METHOD.POST, url, data)
                .then(response => {
                    resolve(response)
                }).catch(error => {
                    console.log(error);
                })
        })
    }

    put(url, data) {
        return new Promise((resolve, reject) => {
            this.api(METHOD.PUT, url, data)
                .then(response => {
                    resolve(response)
                }).catch(error => {
                    console.log(error);
                })
        })
    }

    delete(url, data) {
        return new Promise((resolve, reject) => {
            this.api(METHOD.DELETE, url, data)
                .then(response => {
                    resolve(response)
                }).catch(error => {
                    console.log(error);
                })
        })
    }

    api(method, url, data) {
        return new Promise((resolve, reject) => {

            let axiosConfig = {};
            axiosConfig.method = method;
            axiosConfig.url = this.baseURL + url;

            axiosConfig.headers = this.setHeaders(data);
            if (data) {
                if (data.params)
                    axiosConfig.params = data.params;

                if (data.data)
                    axiosConfig.data = data.data;
            }

            axios(axiosConfig)
                .then(response => {
                    if (response.data && response.data.status === 500) {
                        notification.error({
                            message : 'Error',
                            description : 'Something went wrong.'
                        })
                    } else {
                        resolve(response.data);
                    }
                })
                .catch(error => {
                    console.log("ERROR", error);
                    //DEFAULT ERROR HANDLING
                })
        })
    }

    setHeaders(data) {
        let headers = {}
        headers['accept-language'] = 'en';
        headers['Content-Type'] = 'application/json';
        headers['Device'] = 'ewogICJBcHBWZXJzaW9uQ29kZSIgOiAiMSIsCiAgIkFwcFNpZ25hdHVyZSIgOiAiIiwKICAiTWFudWZhY3R1cmVyIiA6ICJBcHBsZSIsCiAgIk9TVmVyc2lvbiIgOiAiMTIuNCIsCiAgIlNlcmlhbE5vIiA6ICIiLAogICJBcHBWZXJzaW9uIiA6ICIzLjEuNiIsCiAgIlVVSUQiIDogIjNEMDI5RkU0LUY1MTUtNDUxNC1COTNELTE4Qzc0RTcwQTg3QyIsCiAgIk9TIiA6ICJpT1MiLAogICJNb2RlbCIgOiAiU2ltdWxhdG9yIiwKICAiUGxhdGZvcm0iIDogIk1vYmlsZSIKfQ==';

        if (data) {
            if (data.isMultipart) {
                headers['Content-Type'] = 'multipart/form-data'
            }

            if (data.headers) {
                for (var key in data.headers) {
                    if (data.headers.hasOwnProperty(key)) {
                        headers[key] = data.headers[key];
                    }
                }
            }
        }

        if (this.isLoggedIn !== false && (!(data && data.skipAuth)) && (this.userToken)) {
            headers['Authorization'] = `Bearer ${this.userToken}`;
        }

        return headers;
    }
}


export default API;
