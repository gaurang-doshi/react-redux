import React from 'react';
import { BrowserRouter } from 'react-router-dom'
import BaseContainer from './Components/BaseContainer'

// CSS
import 'antd/dist/antd.css';

class App extends React.Component{

    render(){
        return(
            <BrowserRouter basename={process.env.REACT_APP_BASE_PATH}>
                <BaseContainer />
            </BrowserRouter>
        )
    }
}

export default App