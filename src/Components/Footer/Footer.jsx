import React from 'react'
import { FooterContainer } from './Footer.style'
import { Layout } from 'antd'

export const Footer = () => (
    <FooterContainer>
        <Layout.Footer>Design ©2019</Layout.Footer>
    </FooterContainer>
)
