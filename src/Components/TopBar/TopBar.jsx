import React from 'react'
import { Layout, Menu, Icon  } from 'antd'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import * as URL from './../../Helpers/Paths'

import Login from './../Login/Login'
import { TopBarContainer } from './TopBar.style'
import { ACTION_TYPE } from './../../Redux/Auth/Actions';

const { Header } = Layout

class TopBar extends React.Component{
    
    constructor(props){
        super(props)

        this.state = {
            loginModalStatus : false
        }
    }

    render(){
        console.log('props', this.props)
          
        return(
            <TopBarContainer>
                <Header>
                    <div className="logo" />
                    <Menu
                        theme="dark"
                        mode="horizontal"
                        defaultSelectedKeys={['1']}
                        style={{ lineHeight: '64px' }}
                    >
                        <Menu.Item key="1" onClick={() => this.navigateToURL(URL.URL_BASE)}>Home</Menu.Item>
                        <Menu.Item key="2" onClick={() => this.navigateToURL()}>About</Menu.Item>
                        {(!this.props.isLoggedIn) ? 
                                <Menu.Item className="pull-right" key="3" onClick={this.openLoginModel}>Login</Menu.Item>
                            :
                            
                            <Menu.SubMenu
                                className="pull-right"
                                key="5"
                                title={
                                    <span>
                                    <Icon type="user" />
                                <span>{this.props.userName}</span>
                                    </span>
                                }>
                                    <Menu.Item key="6" onClick={() => this.navigateToURL()}>Profile</Menu.Item>
                                    <Menu.Item key="7" onClick={this.logoutUser}>Logout</Menu.Item>
                            </Menu.SubMenu>
                            
                        }
                    </Menu>
                </Header>

                <Login 
                    loginModalStatus={this.state.loginModalStatus}
                    closeLoginModal={this.closeLoginModal} />
            </TopBarContainer>
        )
    }

    navigateToURL = (URL) => {
        console.log("TCL: TopBar -> navigateToURL -> URL", URL)
        this.props.history.push(URL)
    }

    openLoginModel = () => {
        this.setState({
            loginModalStatus : true
        })
    }

    closeLoginModal = () => {
        this.setState({
            loginModalStatus : false
        })
    }

    logoutUser = () => {
        this.props.history.push(URL.URL_BASE)
        this.props.dispatch({ type : ACTION_TYPE.LOGOUT_USER})
    }
}

const mapStateWithProps = state => {
    console.log("TCL: state", state)
    return {
        isLoggedIn : state.Auth.isLoggedIn,
        userName : (state.Auth.userData) ? state.Auth.userData.userName : ''
    }
}


export default connect(mapStateWithProps, null)(withRouter(TopBar));