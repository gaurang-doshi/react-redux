
import styled from 'styled-components'

export const LandingContainer = styled.div`
    display : flex;
    align-items : center;
    justify-content : center;
    padding : 25px;
    .item{
        width : 50%;
    }
`   