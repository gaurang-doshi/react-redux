import React from 'react';
import { Link } from 'react-router-dom'
import { Form, Icon, Input, Button, Checkbox, Modal } from 'antd';
import { LoginContainer } from './Login.style'
import { connect } from 'react-redux';

import { ACTION_TYPE } from './../../Redux/Auth/Actions';

class Login extends React.Component{

    handleCancel = () => {
        this.props.closeLoginModal()

    };

    handleLogin = (e) => {
        e.preventDefault()
        this.props.form.validateFields((err, values) => {
        console.log("TCL: Login -> handleLogin -> this.props", this.props)
            if (!err) {
                console.log('Received values of form: ', values);

                // API call goes here
                // After Success of API 
                this.props.dispatch({
                    type : ACTION_TYPE.LOGIN_USER,
                    userData : {
                        userName : values.username,
                        id: 1
                    },
                    userToken : 'ABC123456', // it will replace by API response
                    isLoggedIn : true
                })

                this.props.closeLoginModal()
                
            }
        });

    }

    render(){

        const { getFieldDecorator } = this.props.form;
        return(
            <Modal
                title="Login"
                visible={this.props.loginModalStatus}
                onCancel={this.handleCancel}
                footer = {null}
            >
                <LoginContainer>
                    <Form className="login-form" onSubmit={this.handleLogin}>
                        <Form.Item>
                            {getFieldDecorator('username', {
                                rules: [{ required: true, message: 'Please input your username!' }],
                            })(
                                <Input
                                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                    placeholder="Username"
                                />,
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('password', {
                                rules: [{ required: true, message: 'Please input your Password!' }],
                            })(
                                <Input
                                    prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                    type="password"
                                    placeholder="Password"
                                />,
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('remember', {
                                    valuePropName: 'checked',
                                    initialValue: true,
                            })(<Checkbox>Remember me</Checkbox>)}
                            <Button type="primary" htmlType="submit" className="login-form-button">
                                Log in
                            </Button>
                        </Form.Item>
                    </Form>
                </LoginContainer>
            </Modal>
        )
    }
}


const WrappedLogin = Form.create()(Login);
export default connect(null, null)(WrappedLogin)