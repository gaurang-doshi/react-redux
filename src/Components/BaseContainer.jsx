import React from 'react';
import Routes from './../Routes/Routes'

import TopBar from './TopBar/TopBar'
import { Footer } from './Footer/Footer'

// STYLE 
import { GlobalContainer } from './../Assets/Styles/global.style'

class BaseContainer extends React.Component{
    render(){

        return(
            <GlobalContainer>
                <TopBar />
                <Routes />
                <Footer />
            </GlobalContainer>
        )
        
    }

}

export default  BaseContainer;